#!/usr/bin/env texlua
-- Build script for kvmap

module = "kvmap"
stdengine = "luatex"
unpackfiles = { "kvmap.dtx" }
typesetexe = "lualatex"
typesetopts = "--shell-escape"
checkopts = ""

-- tagfiles = { "*.dtx", "README.md" }
uploadconfig  = {
  pkg         = module,
  author      = "Island of TeX",
  license     = "lppl1.3c",
  summary     = "Create Karnaugh maps with LaTeX",
  topic       = "engineering",
  ctanPath    = "/macros/latex/contrib/kvmap",
  repository  = "https://gitlab.com/islandoftex/texmf/kvmap",
  bugtracker  = "https://gitlab.com/islandoftex/texmf/kvmap/-/issues",
  update      = true,
  description = [[
This LaTeX package allows the creation of (even large) Karnaugh maps. It
provides a tabular-like input syntax and support for drawing bundles
(implicants) around adjacent values. It is based on an [answer at
StackExchange](https://tex.stackexchange.com/a/425135).
  ]]
}

